<?php
include_once './vendor/autoload.php';

use examAppsadmin\Users\Users;

$filepath = realpath(dirname(__FILE__));
include_once './inc/header.php';

$userObj = new Users();
$data = $userObj->RecyclebinView();
//print_r($data);
if (isset($_SESSION['user'])) {
    ?>

    <div class="main">
        <?php if(isset($data)&& !empty($data)){ ?>
        <h1>Recycle Bin
            <span style="float: right;">
                <?php
                if (isset($_SESSION['msg'])) {
                    echo $_SESSION['msg'];
                    unset($_SESSION['msg']);
                }
                ?>
            </span>
        </h1>

        <div class="manageUser">
            <table class="tblone" style="text-align: center">
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>User Name</th>
                    <th>E-mail</th>
                    <th>Password</th>
                    <th>Action</th>
                </tr>

                <?php
                $id = 0;
                foreach ($data as $value) {
                    ?>
                    <tr>
                        <td><?php echo ++$id; ?></td>
                        <td><?php echo $value['name']; ?></td>
                        <td><?php echo $value['userName']; ?></td>
                        <td><?php echo $value['email']; ?></td>
                        <td><?php echo $value['userPass']; ?></td>
                        <td><?php $status = $value['status'];
                           
                            ?>
                            <a href="users.php">↩</a>
                            <a href="Restore.php?unique_id=<?php echo $value['unique_id']; ?>"><span style="font-size: 26px;"><img src="img/restore.png"/></span></a>
                            <a onclick="return confirm('Are You sure You want to delete...?')" href="userdelete.php?unique_id=<?php echo $value['unique_id']; ?>"><img src="img/delete_1.png" href="#" /></a>
                          
                          
                        </td>
                    </tr>
                <?php }
                ?>

            </table>
        </div>

        <?php }  else { ?>
        <span style="color: blue; text-align: center;">Recycle Bin is Empty....<br> <a href="users.php">Go back User list</a></span>
          <?php unset($_SESSION['msg']); } ?>
    </div>
    <?php
    include 'inc/footer.php';
} else {
    header('location:login.php');
}
?>
<style>
    a{
        text-decoration: none;
        font-size: 26px;
    }
    a:hover{
        color: #00ff99;
    }
</style>