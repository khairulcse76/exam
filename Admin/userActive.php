<?php
include_once './vendor/autoload.php';

use examAppsadmin\Users\Users;

$userObj = new Users();
$data = $userObj->prepare($_GET)->userActive();

if($data){
     $_SESSION['msg']='<b style=" color:green; font-size: 16px;">User Now Active...</b>';
    header('location:users.php');
}
