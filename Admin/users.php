<?php
include_once './vendor/autoload.php';

use examAppsadmin\Users\Users;

$filepath = realpath(dirname(__FILE__));
include_once './inc/header.php';

$userObj = new Users();
$data = $userObj->userDeails();
if (isset($_SESSION['user'])) {
    
    ?>

    <div class="main">
        <h1>User List
             <span style="float: right; margin-left: 35px;">
               <a href="RecycleBin.php"><img src="img/RecycleBinr.png"/></a>
            </span>
            <span style="float: right;">
                <?php
                if (isset($_SESSION['msg'])) {
                    echo $_SESSION['msg'];
                    unset($_SESSION['msg']);
                }
                ?>
            </span>
           
        </h1>

        <div class="manageUser">
            <?php if(isset($data) && !empty($data)){?>
            <table class="tblone" style="text-align: center">
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>User Name</th>
                    <th>E-mail</th>
                    <th>Password</th>
                    <th>Action</th>
                </tr>
                
                <?php
                $id = 0;
                foreach ($data as $value) {
                    ?>
                    <tr>
                        <td><?php echo ++$id; ?></td>
                        <td><?php echo $value['name']; ?></td>
                        <td><?php echo $value['userName']; ?></td>
                        <td><?php echo $value['email']; ?></td>
                        <td><?php echo $value['userPass']; ?></td>
                        <td><?php
                            $status = $value['status'];
                            if ($status == 1) {
                                ?>
                            <a onclick="return confirm('Are you sure You want to desable this user....❗')" href="userDeactive.php?unique_id=<?php echo $value['unique_id']; ?>"><img src="img/Active.png"/></a>
                            <?php } else { ?>
                            <a onclick="return confirm('Are You sure You Want to Active this user....!')" href="userActive.php?unique_id=<?php echo $value['unique_id']; ?>"><img src="img/deactivate_red.png"/> </a>
                                <?php
                            }
                            ?>
                            
                            <a href="userProfile.php?unique_id=<?php echo $value['unique_id']; ?>"><img src="img/view-icon.png" href="#" /></a>
                            <a onclick="return confirm('Are You sure You want to delete...?')" href="trush.php?unique_id=<?php echo $value['unique_id']; ?>"><img src="img/sdfasd.png" href="#" /></a>
                            
                          
                        </td>
                    </tr>
                <?php }
                ?>

            </table>
            <?php } else { ?>
            <span style="color: red; text-align: center;">Not registrations any User ....<br> <a href="RecycleBin.php">View Recycle Bin History</a></span>
         <?php   }?>
        </div>


    </div>
    <?php
    include 'inc/footer.php';
} else {
    header('location:login.php');
}
?>
<style>
    a{
        text-decoration: none;
        font-size: 26px;
    }
    a:hover{
        color: #00ff99;
    }
</style>