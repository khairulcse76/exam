<?php

namespace examAppsadmin\Admin;

use PDO;

class Admin {

    public $bd_user = 'root';
    public $bd_pass = '';
    public $adminUser = '';
    public $adminPass = '';

    public function __construct() {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
        $this->connection = new PDO('mysql:host=localhost;dbname=db_exam', $this->bd_user, '');
    }

    public function prepare($data = '') {
        if (array_key_exists('adminUser', $data)) {
            $this->adminUser = $data['adminUser'];
        }
        if (array_key_exists('adminPass', $data)) {
            $this->adminPass = $data['adminPass'];
        }


        return $this;
    }

    public function login() {
        try {
            $query = "SELECT * FROM tbl_admin WHERE adminUser=" . "'" . $this->adminUser . "' " . "AND adminPass=" . "'" . $this->adminPass . "'";
            $stmt = $this->connection->prepare($query);
            $stmt->execute();
            $userdata = $stmt->fetch();

            if ($userdata) {
                $_SESSION['username'] = $this->adminUser;
                $_SESSION['user'] = $userdata;
                $_SESSION['userid'] = $userdata['admin_id'];
                $_SESSION['login_msg'] = '<b style=" color:blue; font-size: 16px;">Login Successfull';
                unset($_SESSION['Alldata']);
                header('location:index.php');
            }  else {
                $_SESSION['emt_msg'] = '<b style=" color:blue; font-size: 16px;">user name or password dose not match....?';
                header('location:login.php');
            }
        } catch (Exception $ex) {
            
        }
    }
    public function logout(){
        unset($_SESSION['username']);
        unset($_SESSION['user']);
        session_destroy();
        header('location:login.php');
    }

}
