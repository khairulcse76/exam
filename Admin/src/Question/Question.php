<?php

namespace examAppsadmin\Question;

use PDO;

class Question {

    public $bd_user = 'root';
    public $bd_pass = '';
    public $adminUser = '';
    public $adminPass = '';
    public $connection = '';
    public $questionNO = '';
    public $question = '';
    public $ans1 = '';
    public $ans2 = '';
    public $ans3 = '';
    public $ans4 = '';
    public $ans = '';

    public function __construct() {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
        $this->connection = new PDO('mysql:host=localhost;dbname=db_exam', $this->bd_user, '');
    }

    public function prepare($data = '') {
        if (array_key_exists('delquestion', $data)) {
            $this->questionNO = $data['delquestion'];
        }
        if (array_key_exists('questionNo', $data)) {
            $this->questionNO = $data['questionNo'];
        }
        if (array_key_exists('question', $data)) {
            $this->question = $data['question'];
        }
        if (array_key_exists('ans1', $data)) {
            $this->ans1 = $data['ans1'];
        }
        if (array_key_exists('ans2', $data)) {
            $this->ans2 = $data['ans2'];
        }
        if (array_key_exists('ans3', $data)) {
            $this->ans3 = $data['ans3'];
        }
        if (array_key_exists('ans4', $data)) {
            $this->ans4 = $data['ans4'];
        }
        if (array_key_exists('ans', $data)) {
            $this->ans = $data['ans'];
        }
        if (array_key_exists('questionNO', $data)) {
            $this->questionNO = $data['questionNO'];
        }



        return $this;
    }

    public function Questionlist() {
        try {
            $query = "SELECT * FROM tbl_question WHERE is_delete=1";
            $stmt = $this->connection->query($query);
            $stmt->execute();
            $row = $stmt->fetchAll();
            return $row;
        } catch (Exception $ex) {
            
        }
    }

    public function RecyclebinView() {
        try {
            $query = "SELECT * FROM tbl_question WHERE is_delete=0";
            $stmt = $this->connection->query($query);
            $stmt->execute();
            $row = $stmt->fetchAll();
            return $row;
        } catch (Exception $ex) {
            
        }
    }

    public function trush() {
        $query = "UPDATE tbl_question a 
            INNER JOIN tbl_ans b 
            ON a.questionNo = b.questionNo 
            SET b.`is_delete` = '0',
            a.is_delete='0'
            WHERE a.questionNo=" . "'" . $this->questionNO . "'";
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute();

        if ($result) {
            header('localhost:Queslist.php');
        }
        return $result;
    }

    public function Restore() {
        $query = "UPDATE tbl_question a 
            INNER JOIN tbl_ans b 
            ON a.questionNo = b.questionNo 
            SET b.`is_delete` = '1',
            a.is_delete='1'
            WHERE a.questionNo=" . "'" . $this->questionNO . "'";
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute();

        if ($result) {
            header('localhost:Queslist.php');
        }
        return $result;
    }

    public function GetQuestionNo() {
         $qeryno = $query = "SELECT * FROM tbl_question";
            $stmtno = $this->connection->prepare($qeryno);
            $stmtno->execute();
            $result=$stmtno->rowCount();
            return $result;
    }

    public function QuestionStore($data1) {
        $ans = array();
        $ans[1] = $data1['ans1'];
        $ans[2] = $data1['ans2'];
        $ans[3] = $data1['ans3'];
        $ans[4] = $data1['ans4'];
        $quesNO = $data1['questionNo'];
        $question = $data1['question'];
        $ansNo = $data1['ans'];
        try {
            $qeryno = $query = "SELECT * FROM tbl_question WHERE questionNo=" . "'" . $quesNO . "'";
            $stmtno = $this->connection->prepare($qeryno);
            $stmtno->execute();
            $row = $stmtno->fetchAll();
            if (isset($row) && !empty($row)) {
                $_SESSION['msg'] = ' <span style=" color:red; font-size:12px; ">Question no Already Exist</span>';
                header('location:quesadd.php');
            } else {
                $query = "INSERT INTO `tbl_question` (`unique_id`, `questionNo`, `question`, `is_delete`)
                                         VALUES(:unique_id, :questionNo, :question, :is_delete)";
                $stmt = $this->connection->prepare($query);
                $insertRow = $stmt->execute(array(
                    ':unique_id' => uniqid(),
                    ':questionNo' => $quesNO,
                    ':question' => $question,
                    ':is_delete' => "1"
                ));

                if ($insertRow) {
                    foreach ($ans as $key => $answer) {
                        if ($answer != '') {
                            if ($ansNo == $key) {

                                $qery = "INSERT INTO `tbl_ans` (`id`, `questionNo`, `rightans`, `ans`, `is_delete`)
                                     VALUES(Null, '$quesNO', '1', '$answer', '1')";
                            } else {

                                $qery = "INSERT INTO `tbl_ans` (`id`, `questionNo`, `rightans`, `ans`, `is_delete`)
                                    VALUES(Null, '$quesNO', '0', '$answer', '1')";
                            }
                            $stmt = $this->connection->prepare($qery);
                            $result = $stmt->execute();

                            if ($result) {
                                continue;
                            } else {
                                die('Error.....');
                            }
                        }
                    }
                    $_SESSION['msg'] = ' <span style=" color:green; font-size:12px; ">Successfully Inserted</span>';
                    unset($_SESSION['Aldata']);
                    header('location:quesadd.php');
                }
            }
        } catch (Exception $ex) {
            
        }
    }

}
