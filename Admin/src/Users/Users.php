<?php

namespace examAppsadmin\Users;

use PDO;

class Users {

    public $bd_user = 'root';
    public $bd_pass = '';
    public $adminUser = '';
    public $adminPass = '';
    public $connection='';
    public $unique_id='';

    public function __construct() {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
        $this->connection = new PDO('mysql:host=localhost;dbname=db_exam', $this->bd_user, '');
    }

    public function prepare($data = '') {
        if (array_key_exists('adminUser', $data)) {
            $this->adminUser = $data['adminUser'];
        }
        if (array_key_exists('adminPass', $data)) {
            $this->aminPass = $data['adminPass'];
        }
        if (array_key_exists('unique_id', $data)) {
            $this->unique_id = $data['unique_id'];
        }


        return $this;
    }

    public function userDeails() {
        try {
            $query = "SELECT * FROM tbl_user WHERE is_delete=1 ORDER BY user_id DESC";
            $stmt = $this->connection->prepare($query);
            $stmt->execute();
            $row = $stmt->fetchAll();
            return $row;
        } catch (Exception $ex) {
            
        }
    }
    public function RecyclebinView() {
        try {
            $query = "SELECT * FROM tbl_user WHERE is_delete=0 ORDER BY user_id DESC";
            $stmt = $this->connection->prepare($query);
            $stmt->execute();
            $row = $stmt->fetchAll();
            return $row;
        } catch (Exception $ex) {
            
        }
    }
   

    public function logout() {
        unset($_SESSION['username']);
        unset($_SESSION['user']);
        session_destroy();
        header('location:login.php');
    }
    public function userdeactive() {
        $query = "UPDATE `tbl_user` SET `status` = '0' WHERE `tbl_user`.`unique_id` ="."'".$this->unique_id."'";
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute();
         return $result;
    }

    public function userActive() {
        $query = "UPDATE `tbl_user` SET `status` = '1' WHERE `tbl_user`.`unique_id` ="."'".$this->unique_id."'";
//        echo $query; die();
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute();
        return $result;
    }
    public function trush(){
        $query="UPDATE `tbl_user` SET `is_delete` = '0' WHERE `tbl_user`.`unique_id` ="."'".$this->unique_id."'";
         $stmt = $this->connection->prepare($query);
        $result = $stmt->execute();
        return $result;
    }
    
     public function RecycleBin(){
        $query="UPDATE `tbl_user` SET `is_delete` = '0' WHERE `tbl_user`.`unique_id` ="."'".$this->unique_id."'";
         $stmt = $this->connection->prepare($query);
        $result = $stmt->execute();
        return $result;
    }
    public function Restore(){
         $query="UPDATE `tbl_user` SET `is_delete` = '1' WHERE `tbl_user`.`unique_id` ="."'".$this->unique_id."'";
         $stmt = $this->connection->prepare($query);
        $result = $stmt->execute();
        return $result;
    }
    public function userdelete(){
         $query= "DELETE FROM `tbl_user` WHERE `tbl_user`.`unique_id` ="."'".$this->unique_id."'";
         $stmt = $this->connection->prepare($query);
        $result = $stmt->execute();
        return $result;
    }
    public function singleView(){
         $query = "SELECT * FROM tbl_user WHERE unique_id="."'".$this->unique_id."'";
            $stmt = $this->connection->prepare($query);
            $stmt->execute();
            $row = $stmt->fetch();
            return $row;
    }

}
