<?php
include_once './vendor/autoload.php';

use examAppsadmin\Question\Question;

$QuesObj = new Question();

$_SESSION['Aldata']=$_POST;

//print_r($_POST);
$questionNo = $_POST['questionNo'];
$question = $_POST['question'];
$ans1 = $_POST['ans1'];
$ans2 = $_POST['ans2'];
$ans3 = $_POST['ans3'];
$ans4 = $_POST['ans4'];
$ans = $_POST['ans'];


    if (empty($questionNo)) {
        $_SESSION['error1'] = 'Please Enter Question no..';
        header('location:quesadd.php');
    }
    if (empty($question)) {
        $_SESSION['error2'] = 'Whats your question..';
        header('location:quesadd.php');
    }
    if (empty($ans1)) {
        $_SESSION['error3'] = 'Please Enter Choice One..';
        header('location:quesadd.php');
    }
    if (empty($ans2)) {
       $_SESSION['error4'] = 'Please Enter Choice Two..';
        header('location:quesadd.php');
    }
    if (empty($ans3)) {
        $_SESSION['error5'] = 'Please Enter Choice Three..';
        header('location:quesadd.php');
    }
    if (empty($ans4)) {
       $_SESSION['error6'] = 'Please Enter Last Choice..';
        header('location:quesadd.php');
    }
    if (empty($ans)) {
        $_SESSION['error7'] = 'Enter ans no Whose choice are Correct..';
        header('location:quesadd.php');
    }else {
    $data=$QuesObj->QuestionStore($_POST);
}