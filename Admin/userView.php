<?php
include_once './vendor/autoload.php';

use examAppsadmin\Users\Users;

$filepath = realpath(dirname(__FILE__));
include_once './inc/header.php';

$userObj = new Users();
$data = $userObj->prepare($_GET)->singleView();
//echo $data['status'];
?>

<div class="main">
    <h1>
        <span style="float: right;">
            <?php
            if (isset($_SESSION['msg'])) {
                echo $_SESSION['msg'];
                unset($_SESSION['msg']);
            }
            ?>
        </span>

    </h1>

    <div class="manageUser">
        <form>
            <table style="color: #333333; background-color: #ccccff; margin: 0 auto; text-align: left; padding: 25px ; border: 2px #ffcc33 solid; margin-top: 50px;">
                <tr>
                    <th>Name</th>
                    <th>:</th>
                    <td><?php echo $data['name']; ?></td>
                </tr>
                <tr>
                    <th>User Name</th>
                    <th>:</th>
                    <td><?php echo $data['userName']; ?></td>
                </tr>
                <tr>
                    <th>Password</th>
                    <th>:</th>
                    <td><?php echo $data['userPass']; ?></td>
                </tr>
                <tr>
                    <th>Status</th>
                    <th>:</th>
                    <td><?php
                        $status = $data['status'];
                        if ($status =='1') {
                            ?>
                        <a><img src="img/Active.png"/> <span style="color: green;">Now Active</span></a>
                        <?php } else { ?>
                        <a><img src="img/deactivate_red.png"/> <span style="color: red;">User Disable</span></a>
                            <?php
                        }
                        ?></td>
                </tr>

                <tr>
                    <td colspan="3">
                        <a href="users.php">Back</a>
                        <a onclick="return confirm('Are You sure You want to delete...?')" href="userdelete.php?unique_id=<?php echo $value['unique_id']; ?>">Delete</a>
                        <a href="userProfile.php">View Profile</a>
                    </td>
                </tr>
            </table>
        </form>
    </div>


</div>
<?php
include 'inc/footer.php';
?>
<style>
    a{
        text-decoration: none;
        font-size: 26px;
    }
    a:hover{
        color: #00ff99;
    }
</style>