<?php

include_once './vendor/autoload.php';

use examAppsadmin\Users\Users;

$userObj = new Users();

$data = $userObj->prepare($_GET)->Restore();

if($data){
    $_SESSION['msg']='<b style=" color:green; font-size: 16px;">User successfully Restored.</b>';
    header('location:RecycleBin.php');
}