<?php

include_once './vendor/autoload.php';

use examAppsadmin\Admin\Admin;

$AdminObject = new Admin();
if (isset($_SESSION['user'])){
    $AdminObject->logout();
}  else {
    header('location:login.php');
}