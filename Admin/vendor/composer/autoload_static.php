<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit571bbf989281b7605e23093958b8bf87
{
    public static $prefixLengthsPsr4 = array (
        'e' => 
        array (
            'examAppsadmin\\' => 14,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'examAppsadmin\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit571bbf989281b7605e23093958b8bf87::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit571bbf989281b7605e23093958b8bf87::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
