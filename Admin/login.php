<?php
session_start();
if (!isset($_SESSION['user'])) {
    include_once './inc/loginheader.php';
    ?>

    <div class="main">
        <h1>Admin Login</h1>
        <div class="adminlogin">
            <form action="checkLogin.php" method="POST">
                <table>
                    <tr>
                        <td>Username</td>
                        <td><input type="text" name="adminUser"
                                   value="<?php
                                   if (isset($_SESSION['Alldata']['adminUser'])) {
                                       echo $_SESSION['Alldata']['adminUser'];
                                       unset($_SESSION['Alldata']['adminUser']);
                                   }
                                   ?>"/></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input type="password" name="adminPass"
                                   value="<?php
                                   if (isset($_SESSION['Alldata']['adminPass'])) {
                                       echo $_SESSION['Alldata']['adminPass'];
                                       unset($_SESSION['Alldata']['adminPass']);
                                   }
                                   ?>"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="Login"/><input type="reset" value="Clear"/></td>
                    </tr>
                    <?php if (isset($_SESSION['emt_msg'])) { ?>
                        <tr>
                            <td colspan="2"style="color:red; font-size: 18px; text-align: center;">
                                <?php
                                echo $_SESSION['emt_msg'];
                                unset($_SESSION['emt_msg']);
                                ?></td>
                        </tr>
                    <?php }
                    ?>
                </table>
            </form>
        </div>
    </div>
    <?php
    include 'inc/footer.php';
} else {
    $_SESSION['login_msg'] = '<b style=" color:red; margin-left:100px; font-size: 16px;">user Already sign in....</b>';
    header('location:index.php');
}?>