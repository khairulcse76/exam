<?php

include_once './vendor/autoload.php';

use examAppsadmin\Users\Users;

$userObj = new Users();

$data = $userObj->prepare($_GET)->trush();

if($data){
    $_SESSION['msg']='<b style=" color:red; font-size: 16px;">User successfully deleted.</b>';
    header('location:users.php');
}