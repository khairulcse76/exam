<?php
include_once './vendor/autoload.php';

use examAppsadmin\Question\Question;

$filepath = realpath(dirname(__FILE__));
include_once './inc/header.php';

$QuesObj = new Question();
$question = $QuesObj->RecyclebinView();
$questionNoCount = $QuesObj->GetQuestionNo();
$insertRow=$questionNoCount+1;

if (isset($_SESSION['user'])) {
    
    ?>

    <div class="main">
        <h1>Question Add
            <span style="float: right;">
                <?php
                if (isset($_SESSION['msg'])) {
                    echo $_SESSION['msg'];
                    unset($_SESSION['msg']);
                }
                ?>
            </span>

        </h1>

        <div class="manageUser">
           
            <form action="saveQuestion.php" method="POST">
                <table>
                    <tr>
                        <td class="cel1">Question No</td>
                        <td class="cel2">:</td>
                        <td class="cel3"><input type="number" name="questionNo" 
                                                value= "<?php if(isset($_SESSION['Aldata']['questionNo'])){ echo $_SESSION['Aldata']['questionNo']; unset($_SESSION['Aldata']['questionNo']); } else{ echo $insertRow; }  ?>"/>
                            <span style="color: red;"><?php if (isset($_SESSION['error1'])) { echo$_SESSION['error1']; unset($_SESSION['error1']);} ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="cel1">Question</td>
                        <td class="cel2">:</td>
                        <td class="cel3"><input type="text" name="question" 
                                                value="<?php if(isset($_SESSION['Aldata']['question'])){ echo $_SESSION['Aldata']['question']; unset($_SESSION['Aldata']['question']); } ?>" 
                                                placeholder="Insert Hare Question"/>
                            <span style="color: red;"><?php if (isset($_SESSION['error2'])) { echo$_SESSION['error2']; unset($_SESSION['error2']);} ?></span>
                        </td>
                    </tr>
                   <tr>
                        <td class="cel1">Choice One</td>
                        <td class="cel2">:</td>
                        <td class="cel3"><input type="text" name="ans1" 
                                                value="<?php if(isset($_SESSION['Aldata']['ans1'])){ echo $_SESSION['Aldata']['ans1']; unset($_SESSION['Aldata']['ans1']); } ?>" 
                                                placeholder="Choice One"/>
                            <span style="color: red;"><?php if (isset($_SESSION['error3'])) { echo$_SESSION['error3']; unset($_SESSION['error3']);} ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="cel1">Choice Two</td>
                        <td class="cel2">:</td>
                        <td class="cel3"><input type="text" name="ans2" 
                                                value="<?php if(isset($_SESSION['Aldata']['ans2'])){ echo $_SESSION['Aldata']['ans2']; unset($_SESSION['Aldata']['ans2']); } ?>" 
                                                placeholder="Choice Two"/>
                            <span style="color: red;"><?php if (isset($_SESSION['error4'])) { echo$_SESSION['error4']; unset($_SESSION['error4']);} ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="cel1">Choice Three</td>
                        <td class="cel2">:</td>
                        <td class="cel3"><input type="text" name="ans3" 
                                                value="<?php if(isset($_SESSION['Aldata']['ans3'])){ echo $_SESSION['Aldata']['ans3']; unset($_SESSION['Aldata']['ans3']); } ?>" 
                                                placeholder="Choice Three"/>
                            <span style="color: red;"><?php if (isset($_SESSION['error5'])) { echo$_SESSION['error5']; unset($_SESSION['error5']);} ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="cel1">Choice Four</td>
                        <td class="cel2">:</td>
                        <td class="cel4"><input type="text" name="ans4" 
                                                value="<?php if(isset($_SESSION['Aldata']['ans4'])){ echo $_SESSION['Aldata']['ans4']; unset($_SESSION['Aldata']['ans4']); } ?>" 
                                                placeholder="Choice Four"/>
                            <span style="color: red;"><?php if (isset($_SESSION['error6'])) { echo$_SESSION['error6']; unset($_SESSION['error6']);} ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="cel1">Correct Ans No</td>
                        <td class="cel2">:</td>
                        <td class="cel3"><input type="number" name="ans" 
                                                value="<?php if(isset($_SESSION['Aldata']['ans'])){ echo $_SESSION['Aldata']['ans']; unset($_SESSION['Aldata']['ans']); } ?>" />
                        <span style="color: red;"><?php if (isset($_SESSION['error7'])) { echo$_SESSION['error7']; unset($_SESSION['error7']);} ?></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="cel1"></td>
                        <td class="cel2"></td>
                         <td class="cel3" colspan="3"><input type="submit" value="Save"/>
                             <input type="reset" value="Clear"/></td>
                    </tr>
                </table>
            </form>
        </div>


    </div>
    <?php
    include 'inc/footer.php';
} else {
    header('location:login.php');
}
?>
<style>
    a{
        text-decoration: none;
        font-size: 26px;
    }
    a:hover{
        color: #00ff99;
    }
    table{
        margin: 0 auto; width: 620px; border: 2px #000099 solid; padding: 25px;
    }
    input[type='number']{
        width: 300px;
        padding: 5px;
    }
    input[type='text']{
        width: 300px;
        padding: 5px;
    }
    input[type='submit']{
        padding: 10px 25px;margin-right: 50px; float: right;
    }
    input[type='submit']:hover, input[type='reset']:hover, input[type='text'], input[type='number']{
        color: #00cc66;
    }
    input[type='reset']{
        padding: 12px 25px; float: right;   margin-right: 30px; 
    }
    .cel1{
    color: #333333; width: 150px; padding: 4px;
    }
    .cel2{
    color: #333333; width: 20px; padding: 4px;
    }
    .cel3{
    color: #333333; width: 370px; padding: 4px;
    }
    .cel1:hover, .cel2:hover, .cel3:hover{
    color: #00cc66;
    }
    
</style>