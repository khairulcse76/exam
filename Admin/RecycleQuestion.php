<?php
include_once './vendor/autoload.php';

use examAppsadmin\Question\Question;

$filepath = realpath(dirname(__FILE__));
include_once './inc/header.php';

$QuesObj = new Question();
$question = $QuesObj->RecyclebinView();
if (isset($_SESSION['user'])) {

    ?>

    <div class="main">
        <h1>Recycle bin
            <span style="float: right;">
                <?php
                if (isset($_SESSION['msg'])) {
                    echo $_SESSION['msg'];
                    unset($_SESSION['msg']);
                }
                ?>
            </span>

        </h1>

        <div class="manageUser">
            <?php if (isset($question) && !empty($question)) { ?>
                <table class="tblone" style="text-align: center">
                    <tr>
                        <th>No</th>
                        <th>Question</th>
                        <th>Action</th>
                    </tr>

                    <?php
                    $id = 0;
                    foreach ($question as $singleQuestion) {
                        ?>
                        <tr>
                            <td><?php echo ++$id; ?></td>
                            <td><?php echo $singleQuestion['question']; ?></td>
                            <td>
                                <a href="?unique_id=<?php echo $singleQuestion['unique_id']; ?>"><img src="img/view-icon.png" href="#" /></a>
                                <a onclick="return confirm('Are You sure You want to delete...?')" 
                                   href="QuesRestore.php?delquestion=<?php echo $singleQuestion['questionNo']; ?>"><img src="img/restore.png" href="#" /></a>
                            </td>
                        </tr>
                    <?php }
                    ?>

                </table>
            <?php } else { ?>
            <span style="color: red; text-align: center;">Recycle bin is Empty ....<br> <a href="queslist.php">View Question List</a></span>
            <?php } ?>
        </div>


    </div>
    <?php
    include 'inc/footer.php';
} else {
    header('location:login.php');
}
?>
<style>
    a{
        text-decoration: none;
        font-size: 26px;
    }
    a:hover{
        color: #00ff99;
    }
</style>