<?php

namespace examApps\register;

use PDO;

class register {

    public $bd_user = 'root';
    public $bd_pass = '';
    public $name = '';
    public $userName = '';
    public $password = '';
    public $email = '';
    public $unique_id = '';
    public $connection = '';
    public $contact = '';
    public $number = '';

    public function __construct() {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
        $this->connection = new PDO('mysql:host=localhost;dbname=db_exam', $this->bd_user, '');
    }

    public function prepare($data = '') {
//        echo '<pre>';
//        print_r($data);
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('userName', $data)) {
            $this->userName = $data['userName'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('contact', $data)) {
            $this->contact = $data['contact'];
        }
        if (array_key_exists('unique_id', $data)) {
            $this->unique_id = $data['unique_id'];
        }


        return $this;
    }

    public function registration() {
        try {
            $checkuser = "SELECT * FROM tbl_user WHERE userName=" . "'" . $this->userName . "'";
            $stmt = $this->connection->prepare($checkuser);
            $stmt->execute();
            $chkResult = $stmt->fetchAll();

            $checkemail = "SELECT * FROM tbl_user WHERE email=" . "'" . $this->email . "'";
            $emailstmt = $this->connection->prepare($checkemail);
            $emailstmt->execute();
            $emailResult = $emailstmt->fetchAll();

            if (!empty($chkResult) && isset($chkResult)) {
                echo '<span style="color: red;">User Name Already Exist</span>';
                exit();
            } else if (!empty($emailResult) && isset($emailResult)) {
                echo '<span style="color: red;">Email Already Exist</span>';
                exit();
            } else {
                $uniqueid = uniqid();
                $insertQuery = "INSERT INTO `tbl_user` (`unique_id`, `name`, `userName`, `email`, `userPass`,`Contact`,`status`, `is_delete`)
                                     VALUES('$uniqueid', '$this->name', '$this->userName', '$this->email', '$this->password','$this->contact', '1', '1')";
//                echo $insertQuery;
//                die();

                $smtminsert = $this->connection->prepare($insertQuery);
                $insertResult = $smtminsert->execute();
                if ($insertResult) {
                    echo '<span style="color: Green;">Registration Successfull.</span>';
                } else {
                    echo '<span style="color: red;">insert Error</span>';
                }
            }
        } catch (Exception $ex) {
            
        }
    }

    public function login() {
        if (empty($this->email) || empty($this->password)) {
            echo "empty";
            exit();
        } else {
            if (filter_var($this->email, FILTER_VALIDATE_EMAIL) === FALSE) {
                echo "email_error";
                exit();
            } else {
                try {
                    $checkuserQuery = "SELECT * FROM tbl_user WHERE email=" . "'" . $this->email . "' and userPass=" . "'" . $this->password . "'";
                    $checkuser = $this->connection->prepare($checkuserQuery);
                    $checkuser->execute();
                    $LoginResult = $checkuser->fetchAll();
                    if ($LoginResult) {
                        foreach ($LoginResult as $result) {
                            if ($result['status'] == '0') {
                                echo "disable";
                                exit();
                            } else {
                                $_SESSION['user'] = $result['name'];
                                $_SESSION['email'] = $result['email'];
                                $_SESSION['id'] = $result['user_id'];
                            }
                        }
//            echo '<span style="color: Green;">Login Successfull.</span>';
                    } else {
                        echo 'error';
                        exit();
//            echo '<span style="color: red;">E-mail or Password dose not Match....</span>';
                    }
                } catch (Exception $ex) {
                    
                }
            }
        }
    }

    public function logout() {
        unset($_SESSION['username']);
        unset($_SESSION['user']);
        unset($_SESSION['id']);
        header('location:index.php');
    }

    public function ProfileView() {
        $id = $_SESSION['id'];
        try {
            $query = "SELECT * FROM tbl_user WHERE user_id =$id ORDER BY user_id DESC";
            $stmt = $this->connection->prepare($query);
            $stmt->execute();
            $row = $stmt->fetchAll();
//            print_r($row);
            return $row;
        } catch (Exception $ex) {
            
        }
    }

    public function ProfileUpdate() {
        if (empty($this->userName) || empty($this->email)) {
            $_SESSION['empty'] = "<span style='color: red;'>Fill must be not empty...</span>";
            header("location:profileUpdate.php");
        } else {
            if (filter_var($this->email, FILTER_VALIDATE_EMAIL) === FALSE) {
                $_SESSION['empty'] = "<span style='color: red;'>Invalid Email Format</span>";
                header("location:profileUpdate.php");
            } else {
                try {
//                    $updatecchekquery = "SELECT * FROM `tbl_user` WHERE unique_id =" . "'" . $this->unique_id . "' and `name` = '$this->name' and
//                                        `userName` = '$this->userName' and `email` = '$this->email' and `Contact` = '$this->contact'";
////                    echo $updatecchekquery; die();
//
//
//
//                    $stmtcheck = $this->connection->prepare($updatecchekquery);
//                    $checkresult = $stmtcheck->execute();
//                    $checkresult=$stmtcheck->fetchAl/**/l();
////                    print_r($checkresult);
//                    die();
//                    if ($checkresult=1) {
//                        $_SESSION['empty'] = "<span style='color: #00cc66;'>Already up to date</span>";
//                        header("location:profile.php");
//                    } else {

                    $query = "UPDATE `tbl_user` SET `name` = '$this->name', `userName` = '$this->userName', `email` = '$this->email', 
                              `Contact` = '$this->contact' WHERE unique_id =" . "'" . $this->unique_id . "'";
//                        echo $query; die();
                    $stmt = $this->connection->query($query);
                    $update = $stmt->execute();
                    if ($update) {
                        $_SESSION['empty'] = "<span style='color: #00cc66;'>Successfully Profile updated</span>";
                        header("location:profile.php");
                    } else {
                        $_SESSION['empty'] = "<span style='color: red;'>profile Update Fail.....!!!</span>";
                        header("location:profileUpdate.php");
                    }
                } catch (Exception $ex) {
                    
                }
            }
        }
    }

    public function userdeactive() {
        $query = "UPDATE `tbl_user` SET `status` = '0' WHERE `tbl_user`.`unique_id` =" . "'" . $this->unique_id . "'";
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute();
        return $result;
    }

    public function userActive() {
        $query = "UPDATE `tbl_user` SET `status` = '1' WHERE `tbl_user`.`unique_id` =" . "'" . $this->unique_id . "'";
//        echo $query; die();
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute();
        return $result;
    }

    public function trush() {
        $query = "UPDATE `tbl_user` SET `is_delete` = '0' WHERE `tbl_user`.`unique_id` =" . "'" . $this->unique_id . "'";
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute();
        return $result;
    }

    public function RecycleBin() {
        $query = "UPDATE `tbl_user` SET `is_delete` = '0' WHERE `tbl_user`.`unique_id` =" . "'" . $this->unique_id . "'";
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute();
        return $result;
    }

    public function Restore() {
        $query = "UPDATE `tbl_user` SET `is_delete` = '1' WHERE `tbl_user`.`unique_id` =" . "'" . $this->unique_id . "'";
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute();
        return $result;
    }

    public function getquistion() {
        $query = "SELECT * FROM tbl_question";
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        $row = $stmt->fetch();
        return $row;
    }

    public function getrow() {
        $query = "SELECT count(*) FROM tbl_question";
        $stmt = $this->connection->query($query);
        $stmt->execute();
        $row = $stmt->fetchColumn();
        return $row;
    }

    public function getquistionbynum($number) {
        $query = "SELECT * FROM tbl_question WHERE questionNo=$number";
        $stmt = $this->connection->query($query);
        $stmt->execute();
        $row = $stmt->fetch();
        return $row;
    }
    public function getans($number) {
        $query = "SELECT * FROM  tbl_ans WHERE questionNo=$number";
        $stmt = $this->connection->query($query);
        $stmt->execute();
        $row = $stmt->fetchAll();
        return $row;
    }

}
