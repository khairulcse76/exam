<?php

namespace examApps\process;

use PDO;

class process {
    public $bd_user = 'root';
    public $bd_pass = '';
    public $number = '';
    public $ans = '';
    public $connection = '';
    public $contact = '';

    public function __construct() {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
        $this->connection = new PDO('mysql:host=localhost;dbname=db_exam', $this->bd_user, '');
    }
    
    public function prepare($data = '') {
        if (array_key_exists('number', $data)) {
            $this->number = $data['number'];
        }
        if (array_key_exists('ans', $data)) {
            $this->ans = $data['ans'];
        }
        return $this;
    }
    
    public function getnextquestion(){
        $next=  $this->number+1;
        
        if(!isset($_SESSION['score'])){
            $_SESSION['score']='0';
        }
        $total=  $this->getrow();
        $rightans=  $this->getrightans();
        if($rightans==$this->ans){
            $_SESSION['score']++;
        }  
        if($this->number==$total){
            header('location:final.php');
            exit();
        }  else {
            header('location:test.php?q='.$next);
        }
        
    }

    public function getrightans() {
        $query = "SELECT * FROM tbl_ans WHARE WHERE questionNo =$this->number AND rightans = 1";
//        echo $query; die();
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        $row = $stmt->fetch();
        $id=$row['id'];
        return $id;
    }

    public function getrow() {
        $query = "SELECT count(*) FROM tbl_question";
        $stmt = $this->connection->query($query);
        $stmt->execute();
        $row = $stmt->fetchColumn();
        return $row;
    }

}
