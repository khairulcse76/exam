<?php

include_once '../vendor/autoload.php';

use examApps\register\register;

$registerObj = new register();
if($_SERVER['REQUEST_METHOD']=='POST'){
    $registerObj->prepare($_POST)->ProfileUpdate();
}else{
    $_SESSION['empty'] = "<span style='color: red;'>Unauthorized Request</span>";
    header("location:profileUpdate.php");
}
