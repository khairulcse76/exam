$(function () {
    $("#regsubmit").click(function () {
        var name = $("#name").val();
        var userName = $("#userName").val();
        var password = $("#password").val();
        var email = $("#email").val();
        var DataSring = 'name=' + name + '&userName=' + userName + '&password=' + password + '&email=' + email;
        $.ajax({
            type: 'POST',
            url: "getRegister.php",
            data: DataSring,
            success: function (data) {
                $("#msg").html(data);
                $(DataSring).reset();
            }
        });
        return false;
    });
    
    
    $("#getLogin").click(function () {
        var email = $("#email").val();
        var password = $("#password").val();
        var DataSring ='&email=' + email+ '&password=' + password;
        $.ajax({
            type: 'POST',
            url: "getGetLogin.php",
            data: DataSring,
            success: function (data) {
               if($.trim(data)=='empty'){
                   $(".empty").show();
                   setTimeout(function () {
                       $(".empty").fadeOut();
                   }, 3000);
               }else if ($.trim(data)=='email_error'){
                   $(".email_error").show();
                   setTimeout(function () {
                       $(".email_error").fadeOut();
                   }, 3000);
               }else if ($.trim(data)=='disable'){
                   $(".disable").show();
                   setTimeout(function () {
                       $(".disable").fadeOut();
                   }, 3000);
               }else if ($.trim(data)=='error'){
                   $(".error").show();
                   setTimeout(function () {
                       $(".error").fadeOut();
                   }, 3000);
               }else {
                   window.location="exam.php";

               }
            }
        });
        return false;
    });

    $("#update").click(function () {

        var name = $("#name").val();
        var userName = $("#userName").val();
        var contact = $("#contact").val();
        var email = $("#email").val();
        var unique_id = $("#unique_id").val();

        var DataSring ='&name=' + name+ '&userName=' + userName +'&contact=' + contact+ '&email=' + email+ '&unique_id=' + unique_id;
        $.ajax({
            type: 'POST',
            url: "updaterequest.php",
            data: DataSring,
            success: function (data) {
                if($.trim(data)=='empty'){
                    $(".empty").show();
                    setTimeout(function () {
                        $(".empty").fadeOut();
                    }, 3000);
                }else if ($.trim(data)=='email_error'){
                    $(".email_error").show();
                    setTimeout(function () {
                        $(".email_error").fadeOut();
                    }, 3000);
                }else if ($.trim(data)=='error'){
                    $(".error").show();
                    setTimeout(function () {
                        $(".error").fadeOut();
                    }, 3000);
                }else {
                    window.location="profile.php";
                }
            }
        });
        return false;
    });
});