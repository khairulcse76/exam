<?php

include_once '../vendor/autoload.php';

use examApps\register\register;

$registerObj = new register();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $name = $_POST['name'];
    $userName = $_POST['userName'];
    $password = $_POST['password'];
    $email = $_POST['email'];

    if (empty($userName) || empty($password) || empty($email)) {

        if (empty($userName)) {
            echo '<span style="color: red;">Username Must be not Empty...</span><br>';
            
            
        }
        if (empty($password)) {
            echo '<span style="color: red;">Password Must be not Empty...</span><br>';
        }
        if (empty($email)) {
            echo '<span style="color: red;">E-mail Must be not Empty...</span><br>';
        }
    } else {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
            echo '<span style="color: red;">Invalid Email Address...</span>';
            exit();
        } else {
            $registerObj->prepare($_POST)->registration();
        }
    }
}