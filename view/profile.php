<?php
include_once '../vendor/autoload.php';

use examApps\register\register;

$registerObj = new register();

$data = $registerObj->ProfileView();

if (isset($_SESSION['id'])) {

//echo "<pre>";
//    print_r($data);
    include 'inc/header.php';


    ?>

    <div class="main">
        <!--        <h1></h1>-->

        <span class="success" style="color: red; display: none;">update successful</span>
        <div class="segment1" style="margin-right:30px;">
            <div class="segment2">
                <div class="Completecexam">Personal Information</div>
                <div class="Completecexamview">
                    <form action="profileUpdate.php" method="post">

                        <?php
                        foreach ($data as $value) { ?>
                            <table>
                                <?php if (!empty($value['name'])) { ?>
                                    <tr>
                                        <td>Name</td>
                                        <td>:</td>
                                        <td><?php echo $value['name'] ?> </td>
                                    </tr>
                                <?php }
                                if (!empty($value['userName'])) { ?>
                                    <tr>
                                        <td>Usernaem</td>
                                        <td>:</td>
                                        <td><?php echo $value['userName'] ?> </td>
                                    </tr>
                                <?php }
                                if (!empty($value['Contact'])) { ?>
                                    <tr>
                                        <td>Contact</td>
                                        <td>:</td>
                                        <td><?php echo $value['Contact'] ?> </td>
                                    </tr>
                                <?php }
                                if (!empty($value['email'])) { ?>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td><?php echo $value['email'] ?> <input type="hidden"></td>
                                    </tr>
                                <?php } ?>

                                <?php
                                if (isset($_SESSION['empty'])) {
                                    ?>
                                    <tr>
                                        <td colspan="3" style="text-align:center"><?php echo $_SESSION['empty'];
                                            unset($_SESSION['empty']); ?></td>
                                    </tr>
                                <?php } ?>


                                <tr>
                                    <td colspan="3"><input type="submit" value="Update Profile"></td>
                                </tr>

                            </table>

                        <?php } ?>
                    </form>
                </div>
            </div>
            <div class="segment3">
                <div class="Completecexam">Completed Exam</div>
                <div class="Completecexamview">
                    <table>
                        <tr>
                            <th>Exam Name</th>
                            <th>Total Time</th>
                            <th>Status</th>
                        </tr>
                        <tr>
                            <td>PHP</td>
                            <td>20 Munite</td>
                            <td>Pass</td>
                        </tr>
                        <tr>
                            <td>HTML</td>
                            <td>18 Munite</td>
                            <td><input type="checkbox" checked></td>
                        </tr>
                        <tr>
                            <td>PHP</td>
                            <td>20 Munite</td>
                            <td>Pass</td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
        <!--        <div class="segment">-->
        <!--            <form>-->
        <!--                <table>-->
        <!--                    <tr>-->
        <!--                        <td><input type="text" name="slkd"></td>-->
        <!--                        <td><input type="text" name="slkd"></td>-->
        <!--                    </tr>-->
        <!--                </table>-->
        <!--            </form>-->
        <!--        </div>-->

    </div>
    <?php include 'inc/footer.php';
} else {
    echo $_SESSION['user'];
    echo "unauthorize Access...";
} ?>

<style>
    table {
        width: 400px;
        border-collapse: collapse;
    }

    th, td {
        padding: 5px;
        text-align: left;
        border-bottom: 1px solid #ddd;

    }

    .segment1 {
        width: 820px;
        height: 300px;
        margin: 0 auto;
    }

    .segment2 {
        width: 400px;
        border-right: 1px solid rgba(70, 49, 37, 0.96);
        border-left: 1px solid rgba(70, 49, 37, 0.96);
        margin: 0 auto;
        float: left;
    }

    .segment3 {
        width: 400px;
        border-left: 1px solid rgba(70, 49, 37, 0.96);
        border-right: 1px solid rgba(70, 49, 37, 0.96);
        margin: 0 auto;
        float: right;
    }

    .Completecexam {
        padding: 12px;
        background: #d0d0d0;
        color: #0033ff;
        text-align: center;
        text-transform: uppercase;
        /*border-radius: 20px 20px 0px 0;*/
    }

    input[type="submit"] {
        float: right;
        width: 150px;
        margin-right: 50px;
        padding: 8px 15px;
        cursor: pointer;
    }

    input[type="submit"]:hover {
        color: #000099;
    }
</style>
