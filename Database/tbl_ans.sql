-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2016 at 02:05 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_exam`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans`
--

CREATE TABLE `tbl_ans` (
  `id` int(11) NOT NULL,
  `questionNo` int(11) NOT NULL,
  `rightans` int(11) NOT NULL,
  `ans` text NOT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans`
--

INSERT INTO `tbl_ans` (`id`, `questionNo`, `rightans`, `ans`, `is_delete`) VALUES
(1, 1, 1, 'Personal Home Page', 0),
(2, 1, 0, 'Hypertext Preprocessor', 0),
(3, 1, 0, 'Pretext Hypertext Processor', 0),
(4, 1, 0, 'Preprocessor Home Page', 0),
(5, 2, 0, 'Willam Makepiece', 1),
(6, 2, 1, 'Rasmus Lerdorf', 1),
(7, 2, 0, 'Drek Kolkevi', 1),
(8, 2, 0, 'List Barely', 1),
(9, 3, 0, '.html', 1),
(10, 3, 1, '.php', 1),
(11, 3, 0, '.xml', 1),
(12, 3, 0, '.txt', 1),
(13, 4, 0, 'for loop', 1),
(14, 4, 0, 'while loop', 1),
(15, 4, 0, 'foreach loop', 1),
(16, 4, 1, 'do-while loop', 1),
(17, 5, 0, 'Notepad', 1),
(18, 5, 1, 'All of the mentioned.', 1),
(19, 5, 0, 'Notepad++.', 1),
(20, 5, 0, 'Adobe Dreamweaver', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_ans`
--
ALTER TABLE `tbl_ans`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_ans`
--
ALTER TABLE `tbl_ans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
