-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 21, 2016 at 03:56 PM
-- Server version: 10.1.9-MariaDB-log
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_exam`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `unique_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `userName` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `Contact` varchar(20) NOT NULL,
  `userPass` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `unique_id`, `name`, `userName`, `email`, `Contact`, `userPass`, `status`, `is_delete`) VALUES
(1, '215dfgsf65s6s', 'Khairul Islam', 'khairul', 'khairulislamtpi76@gmail.com', '01774460981', '123', 1, 1),
(2, 'kaj36f36d5s', 'Totapakhi', 'totapakhi', 'totapakhi@gmail.com', '', 'totapakhi', 1, 1),
(3, '6g54hg54f6hdfg15', 'Mst. Aysha Islam', 'Aysha ', 'Aysha@gmail.com', '', 'Aysha ', 0, 0),
(5, 'sd4fsdgdfhdf35', 'Tanjila Hayder Urmi', 'Tanjila', 'Tanjila @gamil.com', '', 'Tanjila', 1, 1),
(6, 'sd4fsdgdfdf35f', 'Rashedul Islam', 'Rashedul', 'RashedulIslam@gamil.com', '', 'RashedulIslam', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
